# Plant Computational Genomics Lab
# Author: Alex Trouern-Trend
# Date: May 19th, 2020

library(dplyr)
setwd("/Users/wegrzynlab/Documents/scientist/projects/xmas_NAR/analysis/abscissiongenes/v2_entap")

# Read in annotations
bf0 <- read.csv("./bf_degs/final_annotations_no_contam_lvl0.tsv", sep = "\t", header = TRUE)
ff0 <- read.csv("ff_degs/final_annotations_no_contam_lvl0.tsv", sep = "\t", header = TRUE)
cf0 <- read.csv("./cf_degs/final_annotations_no_contam_lvl0.tsv", sep = "\t", header = TRUE)
of0 <- read.csv("./of3spec_degs/final_annotations_no_contam_lvl0.tsv", sep = "\t", header = TRUE)

# DB hits
sum(bf0$Subject.Sequence != "")
sum(ff0$Subject.Sequence != "")
sum(of0$Subject.Sequence != "")
# hits
sum(bf0$Subject.Sequence != "")/length(bf0$Subject.Sequence)
sum(ff0$Subject.Sequence != "")/length(ff0$Subject.Sequence)
sum(of0$Subject.Sequence != "")/length(of0$Subject.Sequence)
 
# mean ID
mean(bf0$Percent.Identical, na.rm = T)
mean(ff0$Percent.Identical, na.rm = T)
mean(of0$Percent.Identical, na.rm = T)

# mean coverage
mean(bf0$Coverage, na.rm = T)
mean(ff0$Coverage, na.rm = T)
mean(of0$Coverage, na.rm = T)

# mean eval
mean(bf0$E.Value, na.rm = T)
mean(ff0$E.Value, na.rm = T)
mean(of0$E.Value, na.rm = T)

# Distributions
bf_sub <- data.frame(table(bf0$Subject.Sequence, exclude = ""))
ff_sub <- data.frame(table(ff0$Subject.Sequence, exclude = ""))
of_sub <- data.frame(table(of0$Subject.Sequence, exclude = ""))

ggplot(bf_sub, aes(x = Var1, y = Freq)) +
  geom_bar(colour="black", fill="black", stat = "identity") +
  theme_minimal() +
  xlab("Subject Sequence") +
  ggtitle("Balsam DEGs: Hits per Arabidopsis Ortholog") +
  theme(text=element_text(family="mono"),
        axis.text.x = element_text(angle = 50, hjust = 1),
        axis.title = element_blank(),
        legend.title = element_blank(),
        panel.background = element_blank(),
        title = element_text(face = "bold", size = 16))

ggplot(ff_sub, aes(x = Var1, y = Freq)) +
  geom_bar(colour="black", fill="black", stat = "identity") +
  theme_minimal() +
  xlab("Subject Sequence") +
  ggtitle("Fraser DEGs: Hits per Arabidopsis Ortholog") +
  theme(text=element_text(family="mono"),
        axis.text.x = element_text(angle = 50, hjust = 1),
        axis.title = element_blank(),
        legend.title = element_blank(),
        panel.background = element_blank(),
        title = element_text(face = "bold", size = 16))

ggplot(of_sub, aes(x = Var1, y = Freq)) +
  geom_bar(colour="black", fill="black", stat = "identity") +
  theme_minimal() +
  xlab("Subject Sequence") +
  ggtitle("Combined DEGs: Hits per Arabidopsis Ortholog") +
  theme(text=element_text(family="mono"),
        axis.text.x = element_text(angle = 50, hjust = 1),
        axis.title = element_blank(),
        legend.title = element_blank(),
        panel.background = element_blank(),
        title = element_text(face = "bold", size = 16))

# Funct annotation of Orthologs reoccuring...

bf_fforthologs <- intersect(bf_sub$Var1, ff_sub$Var1)

multi <- bf0[bf0$Subject.Sequence %in% bf_fforthologs,]
mm <- data.frame("ortholog" = multi$Subject.Sequence, "annotation" = multi$Description)
