# lab-notebook

### May 13th, 2020

**prepping for hbef git update**
Collecting scripts from last few days of work on trinity genome guided assemblies. The pipeline began with hisat indexing the most recent _Fagus grandifolia_ assembly, then hisat read mapping from two transcrtiptome libraries. Sam files were coordinate sorted and provided to Trinity for reference genoem guided assembly. These newly assembled transcriptomes and previous _de novo_ transcriptomes were aligned to reference genome using GMAP. GMAP alignment statistics were compared using R custom scripts.

**hisatBuild.sh** - created indices for recent _Fagus grandifolia_ assembly, `wegrzyn_assembly_v1_30Mar2020_chrRemoved.fasta`

```bash
#!/bin/bash
#SBATCH --job-name=hisatBuild
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --mail-user=alexander.trouern-trend@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --cpus-per-task=20
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=250G
#SBATCH -o hisat_%j.out
#SBATCH -e hisat_%j.err

module load hisat2/2.2.0

basedir="/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/assembly/ref-based/genomes"
genome="wegrzyn_assembly_v1_30Mar2020_chrRemoved.fasta"

hisat2-build -p 20 -f "$basedir/$genome"  fagr_allmaps

```

**hisat.sh** - Aligns trimmed and QC fastq reads to indexed genome.
- _--dta_ flag reports alignments tailored for transcript assemblers
- _-1_ and _-2_ flags because transcriptome constructed using paired end sequencing. 

```bash
#!/bin/bash
#SBATCH --job-name=hisatfg
#SBATCH -o hisat-%j.out
#SBATCH -e hisat-%j.err
#SBATCH --mail-user=alexander.trouern-trand@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=180G

module load hisat2/2.2.0

orgdir="/labs/Wegrzyn/AcerGenomes/acne"
trimmeddir="/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed"
genomeidx="/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/assembly/ref-based/genomes/fagr_allmaps"

FaFgAl1_1=$trimmeddir"/FaFgAl1_R1_trimmed.fastq"
FaFgAl1_2=$trimmeddir"/FaFgAl1_R2_trimmed.fastq"

SpFgCo1_1=$trimmeddir"/SpFgCo1_R1_trimmed.fastq"
SpFgCo1_2=$trimmeddir"/SpFgCo1_R2_trimmed.fastq"

hisat2 -p 12 --dta -x $genomeidx -1 $FaFgAl1_1 -2 $FaFgAl1_2 -S FaFgAl1.sam
hisat2 -p 12 --dta -x $genomeidx -1 $SpFgCo1_1 -2 $SpFgCo1_2 -S SpFgCo1.sam
```

**samsort.sh** - coordinate sorting output .sam files from hisat.

```bash
#!/bin/bash
#SBATCH --job-name=samsort
#SBATCH --mail-user=alexander.trouern-trend@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=120G
#SBATCH -o samsort_%j.out
#SBATCH -e samsort_%j.err
#SBATCH --partition=general
#SBATCH --qos=general

module load samtools

for i in *Fg**; do
    name=$(echo $i | cut -d'.' -f 1);
    echo "$name";
    echo "$i";
    samtools sort -@ 16 -o ${name}.bam ${name}.sam;
    done;
```
**trinity\_assemble.sh** - assembles transcripts based on hisat transcriptome read to genome mapping.
- _--genome\_guided\_max\_intron_ set max intron length to 10000

```bash
#!/bin/bash
#SBATCH -J trinity_refguided
#SBATCH -o trin_ref_%j.out
#SBATCH -e trin_ref_%j.err
#SBATCH -N 1
#SBATCH --mem=180G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH -n 1
#SBATCH -c 24
#SBATCH --mail-type=ALL
#SBATCH --mail-user=alexander.trouern-trend@uconn.edu

module load trinity/2.8.5
module load samtools

Trinity --genome_guided_bam ./alignments/FaFgAl1.bam --genome_guided_max_intron 10000 --max_memory 10G --CPU 24 --output /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/assembly/ref-based/assembly/trinity_FaFgAl1 > trinity.FaFgAl1.Run.Log.txt

#Trinity --genome_guided_bam ./alignments/SpFgCo1.bam --genome_guided_max_intron 10000 --max_memory 80G --CPU 24 --output /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/assembly/ref-based/assembly/trinity_SpFgCo1 > trinity.SpFgCo1.Run.Log.txt
```

**gmap-build** - construct gmap indices ahead of alignment.

```bash
#!/bin/bash
#SBATCH --job-name=gmap 
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --mem=40G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=alexander.trouern-trend@uconn.edu
#SBATCH -o gmap-build%j.out
#SBATCH -e gmap-build%j.err

module load gmap

gmap_build -D /isg/shared/databases/alignerIndex/plant/HBEF/fagr/assembly_v1_30Mar2020_chrRemoved/inex -d fagr_allmap /isg/shared/databases/alignerIndex/plant/HBEF/fagr/assembly_v1_30Mar2020_chrRemoved/wegrzyn_assembly_v1_30Mar2020_chrRemoved.fasta
```

**gmap.sh** - Align both _de novo_ and genome guided assemblies to _Fagus grandifolia_ genome. _-S_ flag simplifies output to alignment summaries only.

```bash
#!/bin/bash
#SBATCH --job-name=gmap 
#SBATCH -n 8
#SBATCH -N 1
#SBATCH --mem=120G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=alexander.trouern-trend@uconn.edu
#SBATCH -o gmap_FgAl1_%j.out
#SBATCH -e gmap_FgAl1_%j.err


module load gmap

#gmap -D /isg/shared/databases/alignerIndex/plant/HBEF/fagr/assembly_v1_30Mar2020_chrRemoved/index/fgr_allmap -d fagr_allmap --nthreads 8 -S /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/assembly/ref-based/de_novo/assemblies/SpFgCo1.fasta

gmap -D /isg/shared/databases/alignerIndex/plant/HBEF/fagr/assembly_v1_30Mar2020_chrRemoved/index/far_allmap -d fagr_allmap --nthreads 8 -S /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/assembly/ref-based/assembly/gmap/SpFgCo1-GG.fasta
```

